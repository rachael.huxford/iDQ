.. _idq-streaming_evaluate:

idq-streaming_evaluate
####################################################################################################

describe script, which modules it relies upon, etc.

.. program-output:: idq-streaming_evaluate --help
   :nostderr:
