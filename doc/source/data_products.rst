.. _formalism-data-products:

Data Products
####################################################################################################

**ADD DESCRIPTIONS OF HOW DATA ARE DISCOVERABLE AND EXPECTED LATENCIES**

Input
----------------------------------------------------------------------------------------------------

* trigger streams for each auxiliary channel, including :math:`\rho(t)`

Timeseries
----------------------------------------------------------------------------------------------------

* :math:`p_g(t)`
* :math:`\ln\Lambda(t)`
* Rank (raw classifier output)
* False Alarm Probability
* Glitch detection efficiency

Feature Importance and diagnostics
----------------------------------------------------------------------------------------------------

* Categorization metrics based on auxiliary channel information

Monitoring
----------------------------------------------------------------------------------------------------

* Algorithmic health

  * Up-time/Duty-cycle timeseries
  * Calibration
  * ROC Curves

* Impact on searches

  * Measures of :math:`VT` with and without data quality information applied
  * Measures of deadtime introduced using veto conditions implemented within low-latency alerting logic
  * Additional latency introduced by data quality information

* Auxiliary channel and Feature Monitoring

  * Channel event maps
  * PSD snapshots for each auxiliary channel
  * :math:`\rho` histograms and glitchgrams
