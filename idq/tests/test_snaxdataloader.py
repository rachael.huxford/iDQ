import copy
import pytest

import numpy as np

from gwpy.table.filters import in_segmentlist


@pytest.mark.usefixtures("snax_dataloader")
class TestSNAXDataLoader(object):
    """
    Tests several aspects of GstlalDataLoader for a compliant dataset.
    """

    def test_times(self, snax_dataloader_conf):
        assert (
            self.dataloader.start == snax_dataloader_conf["start_time"]
        ), "incorrect start time set"
        assert (
            self.dataloader.end == snax_dataloader_conf["end_time"]
        ), "incorrect end time set"

    def test_segs(self, snax_dataloader_conf):
        assert (
            self.dataloader.segs == snax_dataloader_conf["segs"]
        ), "incorrect segments returned for DataLoader"

    def test_columns(self, snax_dataloader_conf):
        self.dataloader.query(channels=snax_dataloader_conf["channels"])
        assert set(self.dataloader.columns) == set(
            snax_dataloader_conf["columns"]
        ), "incorrect columns retrieved from DataLoader"

    def test_channels(self, snax_dataloader_conf):
        self.dataloader.query(channels=snax_dataloader_conf["channels"])
        assert set(self.dataloader.channels) == set(
            snax_dataloader_conf["channels"]
        ), "incorrect channels stored in DataLoader"

    def test_trigger_query_simple(self, snax_dataloader_conf):
        self.dataloader.query()
        for channel in snax_dataloader_conf["channels"]:
            assert (
                channel in self.dataloader.channels
            ), "channel not present in DataLoader"

    def test_trigger_query_channels(self, snax_dataloader_conf):
        channels2query = snax_dataloader_conf["channels"][1:]
        result = self.dataloader.query(channels=channels2query)
        for channel in channels2query:
            assert (
                channel in result.channels
            ), "channel not present in DataLoader that was requested"

    def test_trigger_query_columns(self, snax_dataloader_conf):
        columns2query = ["time", "snr"]
        result = self.dataloader.query(columns=columns2query)
        for channel in self.dataloader.channels:
            for column in columns2query:
                try:
                    result.features[channel][column]
                except KeyError:
                    raise KeyError(
                        "column not present in DataLoader that was requested"
                    )

    def test_trigger_query_segments(self, snax_dataloader_conf):
        segs2query = copy.copy(snax_dataloader_conf["segs"])
        segs2query.contract(100)
        segs_remainder = snax_dataloader_conf["segs"] - segs2query
        result = self.dataloader.query(segs=segs2query, time="time")
        for channel in self.dataloader.channels:
            result.features[channel] = result.features[channel].filter(
                ("time", in_segmentlist, segs_remainder)
            )
            assert not result.features[
                channel
            ], "segment query result from DataLoader contains data outside of bounds"

    def test_pop(self, snax_dataloader_conf):
        self.dataloader.query(channels=snax_dataloader_conf["channels"])
        test_channel = snax_dataloader_conf["channels"][0]
        test_triggers = self.dataloader.pop(test_channel)
        assert (
            test_channel not in self.dataloader.channels
        ), "popped channel still present in DataLoader"

        triggers = self.dataloader.query(channels=snax_dataloader_conf["channels"])
        assert len(triggers.features[test_channel]) == len(
            test_triggers
        ), "triggers from popped channel and DataLoader do not have the same size"
        assert bool(
            np.asarray(test_triggers == triggers.features[test_channel]).all()
        ), "triggers from popped channel do not match triggers contained in DataLoader"
