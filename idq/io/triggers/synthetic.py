import numpy as np

from ... import configparser
from ... import exceptions
from ... import synthetic
from ... import hookimpl
from . import DataLoader


class MockDataLoader(DataLoader):
    """
    a synthetic data generation object
    """

    _default_columns = synthetic.MOCKCLASSIFIER_COLUMNS
    _allowed_columns = synthetic.MOCKCLASSIFIER_COLUMNS
    _required_kwargs = ["config"]

    _default_prob_nodata = 0.0
    _default_prob_incontiguous = 0.0

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self._parse_config(self.kwargs["config"])
        self._dtype = [(key, float) for key in self.columns]

        if "prob_nodata" not in self.kwargs:
            self.kwargs["prob_nodata"] = self._default_prob_nodata
        if "prob_incontiguous" not in self.kwargs:
            self.kwargs["prob_incontiguous"] = self._default_prob_incontiguous

    def _parse_config(self, path):
        """
        read in parameters from config by delegating to
        idq.synthetic.parse_mockclassifierdata_config
        """
        self._streams, self._channel2streams = configparser.path2streams(
            path, self.segs
        )

    def _query(self, channels=None, bounds=None, verbose=False, **kwargs):
        """
        generates synthetic data on-the-fly based on parameters read from config
        file generates and caches data for all channels declared in config
        instead of trying to only generate the channels requested
        """
        if bounds is None:
            bounds = dict()

        if self._data:
            # handle caching explicitly here because otherwise calls to
            # triggers() without specifying channels will re-generate the random
            # streams. This is special behavior just for the Synthetic data
            # objects so we take care of it here rather than in the parent class
            # relies on the fact that we generate all data for all channels the
            # first time this is called
            synthetic_data = dict(self._data)

        else:
            # generate all streams of synthetic triggers

            # figure out if we want to raise an error
            if np.random.rand() < self.kwargs["prob_nodata"]:
                raise exceptions.NoDataError(self.start, self.end - self.start)

            if np.random.rand() < self.kwargs["prob_incontiguous"]:
                gap_start = self.start + np.random.rand() * (self.end - self.start)
                gap_end = gap_start + np.random.rand()(self.end - gap_start)
                raise exceptions.IncontiguousDataError(gap_start, gap_end, self.end)

            # no errors raised, so we simulate streams
            all_streams = dict((obj.name, obj) for obj in self._streams)

            # map those streams into the appropriate witnesses
            synthetic_data = {}
            # FIXME: should this be channel2streams instead?
            # for channel, streams in self._channel2streams.items():
            for channel, streams in all_streams.items():
                # incrementally add streams to this for concatenation at the end
                arrays = [np.array([], dtype=self._dtype)]
                for stream, jitters in streams:  # add jitter to each observed stream
                    # add jitter and downselect to the columns requested
                    arrays.append(stream.jittered(**jitters)[self.columns])
                synthetic_data[channel] = np.concatenate(tuple(arrays))

            if synthetic.MOCKCLASSIFIER_TIME in self.columns:
                # time-order the data if we've requested the time column
                for channel, val in synthetic_data.items():
                    synthetic_data[channel] = val[
                        val[synthetic.MOCKCLASSIFIER_TIME].argsort()
                    ]

        if channels is not None:
            for chan in channels:  # fill in missing channels
                if chan not in synthetic_data:
                    synthetic_data[chan] = np.array([], dtype=self._dtype)

        # NOTE: we do not filter by "bounds" here because this will happen
        # within ClassifierData.triggers() and we do not expect the memory load
        # associated with generating synthetic data to be large enough that we
        # need to filter at this step
        return synthetic_data


@hookimpl
def get_dataloaders():
    return {
        "synthetic": MockDataLoader,
        "synthetic:single": MockDataLoader,
    }
