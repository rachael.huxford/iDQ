import copy

import numpy as np
from scipy import stats

from . import utils

MOCKCLASSIFIER_TIME = "time"
MOCKCLASSIFIER_COLUMNS = [MOCKCLASSIFIER_TIME, "frequency", "snr"]
MOCKCLASSIFIER_DTYPE = [(key, float) for key in MOCKCLASSIFIER_COLUMNS]

# strip the "_gen" from the end of the class name
KNOWN_DISTRIBS = dict(
    (klass.__name__[:-4], klass) for klass in stats.rv_continuous.__subclasses__()
)


class SyntheticTriggerStream(object):
    """
    generates synthetic streams of triggers
    """

    def __init__(self, name, segs, rate, freq_distrib, snr_distrib):
        self.name = name
        self._segs = segs
        self.rate = rate
        self.freq_distrib = freq_distrib
        self.snr_distrib = snr_distrib

        self._data = None

    @property
    def segs(self):
        return self._segs

    @segs.setter
    def segs(self, new_segs):
        self.flush()
        self._segs = new_segs

    def flush(self):
        self._data = None

    def triggers(self):
        if self._data is None:
            # draw the random times
            times = utils.draw_random_times(self.segs, rate=self.rate)
            n = len(times)
            if n:
                self._data = np.array(
                    zip(
                        times,
                        self.freq_distrib.rvs(size=n),
                        self.snr_distrib.rvs(size=n),
                    ),
                    dtype=MOCKCLASSIFIER_DTYPE,
                )

            else:
                self._data = np.array([], dtype=MOCKCLASSIFIER_DTYPE)

        return self._data

    def jittered(self, **jitters):
        """
        equivalent to triggers() but with randomly drawn jitters
        around the data in this stream
        """
        if self._data is None:
            # generate stream
            self.triggers()

        # make the jittered stream, which is *not* cached
        jittered = copy.copy(self._data)
        n = len(jittered)
        for key, val in jitters.items():
            assert (
                key in MOCKCLASSIFIER_COLUMNS
            ), "jitter key must be one of: " + ", ".join(MOCKCLASSIFIER_COLUMNS)
            jittered[key] += np.random.randn(n) * val

        return jittered


class DistribWrapper(object):
    """
    a very thin wrapper around scipy.stats distributions to aid with the API
    """

    def __init__(self, distrib, *args):
        self._distrib_name = distrib
        self.distrib = KNOWN_DISTRIBS[distrib]()
        self.args = args

    def rvs(self, size=1):
        return self.distrib.rvs(*self.args, size=size)

    # the following are used to make this object pickle-able
    # we have to avoid the scipy.stats objects because they are not picklable
    def __getstate__(self):
        return {"distrib_name": self._distrib_name, "args": self.args}

    def __setstate__(self, state):
        self._distrib_name = state["distrib_name"]
        self.distrib = KNOWN_DISTRIBS[self._distrib_name]()
        self.args = state["args"]
