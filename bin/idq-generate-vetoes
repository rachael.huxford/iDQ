#!/usr/bin/env python

import argparse

from idq import cli
from idq import configparser
from idq import factories
from idq import reports


# command line options
parser = argparse.ArgumentParser()

parser.add_argument("config", metavar="PATH", help="path to configuration")
parser.add_argument("nickname", metavar="NICKNAME")
parser.add_argument("start", metavar="START", type=int, nargs="?")
parser.add_argument("end", metavar="END", type=int, nargs="?")

# parse and validate
args = parser.parse_args()
cli.validate_times(args)

# run
cli.set_up_logger(args)

nickname = args.nickname
config = configparser.path2config(args.config)

# find data products
zoom_series = {
    nickname: reports.find_timeseries(config, args.start, args.end, nickname)
}

dataset = reports.find_datasets(config, args.start, args.end, nickname)
dataset_start = dataset.start
dataset_end = dataset.end

model_ids = {s.model_id for s in zoom_series[nickname]}
models = reports.find_models(
    config, dataset_start, dataset_end, nickname, hashes=model_ids
)

classifier_factory = factories.ClassifierFactory()
classifier = classifier_factory(
    nickname, rootdir=".", **config.classifier_map[nickname]
)

# produce vetoes
for key, model in models.items():
    try:
        model_id = model._model_id
    except AttributeError:
        # old-style classifiers without metadata
        model_id = f"bin{model._run_id}"
    out = f"{nickname}_{args.start}_{args.end}_{model_id}_flags.hdf5"
    model.save_as_data_quality_flags(
        out, dataset, classifier.time, classifier.significance
    )
